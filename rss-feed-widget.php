<?php
/*
Plugin Name: Rss Feed Widget
Plugin URI: https://bitbucket.org/sf-repos/wordpress-dashboard-rss-feed-widget-mu-plugin/
Description: A basic RSS Feed Widget
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
Text Domain: rss-feed-widget
License: GPLv2
*/

add_action( 'wp_dashboard_setup', 'rfw_add_dashboard_widget' );

  /**
  * call function to create our dashboard widgete
  */
  function rfw_add_dashboard_widget() {

  wp_add_dashboard_widget(
  'rfw_dashboard_widget',
  'Read our latest news',
  'rfw_create_dashboard_widget'
  );

  }

  /**
  * function to display our dashboard widget content
  */
  function rfw_create_dashboard_widget() {
  ?>
    <h2><?php _e( 'Recent news from Our Blog:', 'my-text-domain' ); ?></h2>

    <?php // Get RSS Feed(s)
    include_once( ABSPATH . WPINC . '/feed.php' );

    // Get a SimplePie feed object from the specified feed source.
    $rss = fetch_feed( 'http://sam-fullalove.uk.to/feed/' );

    if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

        // Figure out how many total items there are, but limit it to 5.
        $maxitems = $rss->get_item_quantity( 5 );

        // Build an array of all the items, starting with element 0 (first element).
        $rss_items = $rss->get_items( 0, $maxitems );

    endif;
    ?>

    <ul>
        <?php if ( $maxitems == 0 ) : ?>
            <li><?php _e( 'No items', 'my-text-domain' ); ?></li>
        <?php else : ?>
            <?php // Loop through each feed item and display each item as a hyperlink. ?>
            <?php foreach ( $rss_items as $item ) : ?>
                <li>
                    <a href="<?php echo esc_url( $item->get_permalink() ); ?>"
                        title="<?php printf( __( 'Posted %s', 'my-text-domain' ), $item->get_date('j F Y | g:i a') ); ?>">

                        <?php
                        //display hyperlinked title
                        echo esc_html( $item->get_title() );?>
                    </a>
                        <?php
                        //display a cut down description and a hyperlinked read more link
                        echo substr($item->get_description(), 0, 50)."...<a href=".esc_url($item->get_permalink()).">Read more</a>";?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>

  <?php
  }
?>
